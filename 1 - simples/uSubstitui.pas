unit uSubstitui;

interface

uses
  uISubstitui;

type
  TSubstitui = class(TInterfacedObject, ISubstitui)
  public
    function Substituir(Str, Velho, Novo: String): String;
  end;

implementation

uses
  System.SysUtils;

{ TSubstitui }

function TSubstitui.Substituir(Str, Velho, Novo: String): String;
var
  bProcurar: Boolean;
  nPos: Integer;
  nPosDelete: Integer;
  nLengthVelho: Integer;
  sStringOriginal: string;
begin
  Result := EmptyStr;

  if (Length(Str) = 0) then
    Exit;

  sStringOriginal := Str;
  nLengthVelho := Length(Velho);
  bProcurar := True;

  while bProcurar do
  begin
    nPos := Pos(Velho, sStringOriginal);
    if (nPos > 0) then
    begin
      Result := Result + Copy(sStringOriginal, 0, nPos-1);
      Result := Result + Novo;
      nPosDelete := nPos + nLengthVelho;
      Delete(sStringOriginal, 1, nPosDelete-1);
    end
    else
      bProcurar := False;
  end;

  Result := Result + sStringOriginal;
end;

end.

