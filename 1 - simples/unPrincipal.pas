unit unPrincipal;

{
-> O referido teste foi realizado usando o Delphi 10 Seattle.

Foram implementadas duas classes:
-> TSubstitui: A qual atende ao enunciado da quest�o para realizar a substitui��o
das strings sem utilizar a API do Delphi.
-> TSubstituiStringReplace: Usando a fun��o StringReplace para realizar a
substitui��o das strings.
}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TfrmPrincipal = class(TForm)
    grBxSubstituir: TGroupBox;
    lblVelha: TLabel;
    lblNova: TLabel;
    lblStringSubstituir: TLabel;
    edVelha: TEdit;
    edNova: TEdit;
    mmStringSubstituir: TMemo;
    mmResultado: TMemo;
    lblResultado: TLabel;
    pnlBotoes: TPanel;
    btnLimpar: TBitBtn;
    btnSubstituir: TBitBtn;
    btnSubstituirStringReplace: TBitBtn;
    bvlDivisoria: TBevel;
    btnFechar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnSubstituirStringReplaceClick(Sender: TObject);
    procedure btnSubstituirClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
  private
    procedure LimparTela;
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses
  uSubstitui, uSubstituiStringReplace;

{ TfrmPrincipal }

procedure TfrmPrincipal.LimparTela;
begin
  mmStringSubstituir.Clear;
  edVelha.Clear;
  edNova.Clear;
  mmResultado.Clear;
  if mmStringSubstituir.CanFocus then
    mmStringSubstituir.SetFocus;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  LimparTela;
end;

procedure TfrmPrincipal.btnLimparClick(Sender: TObject);
begin
  LimparTela;
end;

procedure TfrmPrincipal.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmPrincipal.btnSubstituirClick(Sender: TObject);
var
  oSubstitui: TSubstitui;
begin
  oSubstitui := TSubstitui.Create;
  try
    mmResultado.Clear;
    mmResultado.Text := oSubstitui.Substituir(mmStringSubstituir.Text,
      edVelha.Text, edNova.Text);
  finally
    FreeAndNil(oSubstitui);
  end;
end;

procedure TfrmPrincipal.btnSubstituirStringReplaceClick(Sender: TObject);
var
  oSubstituiStringReplace: TSubstituiStringReplace;
begin
  oSubstituiStringReplace := TSubstituiStringReplace.Create;
  try
    mmResultado.Clear;
    mmResultado.Text := oSubstituiStringReplace.Substituir(mmStringSubstituir.Text,
      edVelha.Text, edNova.Text);
  finally
    FreeAndNil(oSubstituiStringReplace);
  end;
end;

end.
