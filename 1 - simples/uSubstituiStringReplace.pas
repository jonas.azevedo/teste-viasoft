unit uSubstituiStringReplace;

interface

uses
  uISubstitui;

type
  TSubstituiStringReplace = class(TInterfacedObject, ISubstitui)
  public
    function Substituir(Str, Velho, Novo: String): String;
  end;


implementation

uses
  System.SysUtils;

{ TSubstituiStringReplace }

function TSubstituiStringReplace.Substituir(Str, Velho, Novo: String): String;
begin
  Result := StringReplace(Str, Velho, Novo, [rfReplaceAll, rfIgnoreCase]);
end;

end.

