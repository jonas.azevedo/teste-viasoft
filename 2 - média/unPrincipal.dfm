object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'M'#225'quina de Dinheiro'
  ClientHeight = 436
  ClientWidth = 685
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object mmDadosTroco: TMemo
    Left = 0
    Top = 108
    Width = 685
    Height = 276
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 102
  end
  object pnlBotoes: TPanel
    Left = 0
    Top = 384
    Width = 685
    Height = 52
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 390
    object btnLimpar: TBitBtn
      Left = 0
      Top = 0
      Width = 120
      Height = 52
      Align = alLeft
      Caption = '&Limpar'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = btnLimparClick
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitHeight = 64
    end
    object btnFechar: TBitBtn
      Left = 565
      Top = 0
      Width = 120
      Height = 52
      Align = alRight
      Caption = '&Fechar'
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 1
      OnClick = btnFecharClick
      ExplicitLeft = 680
      ExplicitTop = 1
      ExplicitHeight = 64
    end
  end
  object pnlValorTroco: TPanel
    Left = 0
    Top = 0
    Width = 685
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 804
    object lblValorTroco: TLabel
      Left = 12
      Top = 39
      Width = 89
      Height = 19
      Caption = 'Valor Troco:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edValorTroco: TEdit
      Left = 116
      Top = 31
      Width = 121
      Height = 27
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 'edValorTroco'
      OnKeyPress = edValorTrocoKeyPress
    end
    object btnCalcularTrocar: TBitBtn
      Left = 252
      Top = 6
      Width = 169
      Height = 52
      Caption = 'Calcular Trocar'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCalcularTrocarClick
    end
  end
  object pnlResultado: TPanel
    Left = 0
    Top = 73
    Width = 685
    Height = 35
    Align = alTop
    BevelKind = bkTile
    BevelOuter = bvNone
    Caption = 'Resultado:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    ExplicitTop = 79
  end
end
