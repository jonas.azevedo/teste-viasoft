unit uMaquinaDinheiro;

interface

uses
  Classes, uIMaquina, uTroco;

type
  TMaquinaDinheiro  = class(TInterfacedObject, IMaquina)
  private
    procedure CalcularQuantidadeTroco(const poCedula: TCedula; var pnValor: Integer; var poLista: TList);
  public
    function MontarTroco(Troco: Double): TList;
  end;

implementation

uses
  System.SysUtils;

{$REGION 'CalcularQuantidadeTroco'}
procedure TMaquinaDinheiro.CalcularQuantidadeTroco(const poCedula: TCedula;
  var pnValor: Integer; var poLista: TList);
var
  nDiv: Integer;
  nValorCedula: Integer;
  oTroco: TTroco;
begin
  if (poCedula in [ceMoeda50, ceMoeda25, ceMoeda10, ceMoeda5, ceMoeda1]) then
    nValorCedula := Trunc(CValorCedula[poCedula] * 100)
  else
    nValorCedula := Trunc(CValorCedula[poCedula]);

  nDiv := pnValor div nValorCedula;
  if (nDiv <> 0) then
  begin
    oTroco := TTroco.Create(poCedula, nDiv);
    poLista.Add(oTroco);
    pnValor := pnValor mod nValorCedula;
  end;
end;
{$ENDREGION}

{$REGION 'MontarTroco'}
function TMaquinaDinheiro.MontarTroco(Troco: Double): TList;
var
  nValor: Integer;
begin
  Result := TList.Create;

  nValor := Trunc(Troco);
  CalcularQuantidadeTroco(ceNota100, nValor, Result);
  CalcularQuantidadeTroco(ceNota50, nValor, Result);
  CalcularQuantidadeTroco(ceNota20, nValor, Result);
  CalcularQuantidadeTroco(ceNota10, nValor, Result);
  CalcularQuantidadeTroco(ceNota5, nValor, Result);
  CalcularQuantidadeTroco(ceNota2, nValor, Result);
  CalcularQuantidadeTroco(ceMoeda100, nValor, Result);

  nValor := Round(Frac(Troco) * 100);
  CalcularQuantidadeTroco(ceMoeda50, nValor, Result);
  CalcularQuantidadeTroco(ceMoeda25, nValor, Result);
  CalcularQuantidadeTroco(ceMoeda10, nValor, Result);
  CalcularQuantidadeTroco(ceMoeda5, nValor, Result);
  CalcularQuantidadeTroco(ceMoeda1, nValor, Result);
end;
{$ENDREGION}

end.

