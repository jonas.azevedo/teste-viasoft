unit uExceptions;

interface

uses
  System.SysUtils;

type
  TExceptionConexaoJahExistente = class(Exception);
  TExceptionQuantidadeItensNetwork = class(Exception);
  TExceptionValorInvalidoElemento = class(Exception);
  TExceptionValoresIguaisElementos = class(Exception);

implementation

end.
