program prjQuestao03;

uses
  Vcl.Forms,
  unPrincipal in 'unPrincipal.pas' {frmPrincipal},
  uNetwork in 'uNetwork.pas',
  uExceptions in 'uExceptions.pas',
  uConexao in 'uConexao.pas',
  uConstantesMensagens in 'uConstantesMensagens.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
