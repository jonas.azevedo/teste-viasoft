unit uConstantesMensagens;

interface

const
{$REGION 'Mensagens'}
  cMSG_CONEXAO_ENTRE_ELEMENTOS_REALIZADA = 'Conex�o entre os elementos %s e %s realizada com sucesso!';
  cMSG_ELEMENTOS_CONECTADOS_DIRETAMENTE_CONEXAO = 'Elementos %s e %s conectados diretamente pela conex�o: %s e %s';
  cMSG_ELEMENTOS_CONECTADOS_INDIRETAMENTE_CONEXAO = 'Elementos %s e %s conectados indiretamente.';
  cMSG_NAO_FOI_POSSIVEL_ACHAR_CONEXAO_ENTRE_ELEMENTOS = 'N�o foi poss�vel achar conex�o entre os elementos: %s e %s';
  cMSG_NETWORK_CRIADA_COM_ITENS = 'Network criada com %s itens!';
  cMSG_ELEMENTOS_ASSOCIACAO = 'Associa��o: %s e %s';
{$ENDREGION}

implementation

end.
