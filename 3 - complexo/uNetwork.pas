unit uNetwork;

interface

uses
  Generics.Collections, uConexao;

type

  TNetwork = class
  strict private
    FnQuantidadeElementos: Integer;
    FoListaConexoes: TObjectList<TConexao>;
  private
    function VerificarConexaoJahExiste(const pnElemento1: Integer; const pnElemento2: Integer): Boolean;
    function VerificarElementosEstaoAssociadosIndiretamente(
      const poListaElementosAssociativos: TList<Integer>; const pnElementoComparacao: Integer;
      out psMensagemElementosAssociacao: string): Boolean;
    procedure ValidarValoresElementos(const pnElemento1: Integer; const pnElemento2: Integer);
  public
    constructor Create(const pnQuantidadeElementos: Integer); reintroduce;
    destructor Destroy; override;
    function Conectar(const pnElemento1: Integer; const pnElemento2: Integer): Boolean;
    function Consultar(const pnElemento1: Integer; const pnElemento2: Integer;
      out psMensagem: string): Boolean;
    property QuantidadeElementos: Integer read FnQuantidadeElementos;
    property ListaConexoes: TObjectList<TConexao> read FoListaConexoes;
  end;

implementation

uses
  System.SysUtils, uConstantesMensagens, uExceptions;

constructor TNetwork.Create(const pnQuantidadeElementos: Integer);
begin
  if (pnQuantidadeElementos <= 1) then
    raise TExceptionQuantidadeItensNetwork.Create('Quantidade de Elementos deve ser maior que 1');
  FnQuantidadeElementos := pnQuantidadeElementos;
  FoListaConexoes := TObjectList<TConexao>.Create;
  inherited Create;
end;

destructor TNetwork.Destroy;
begin
  FreeAndNil(FoListaConexoes);
  inherited Destroy;
end;

function TNetwork.VerificarConexaoJahExiste(const pnElemento1: Integer; const pnElemento2: Integer): Boolean;
var
  oConexao: TConexao;
begin
  Result := False;
  for oConexao in FoListaConexoes do
  begin
    if (pnElemento1 = oConexao.Elemento1) and (pnElemento2 = oConexao.Elemento2) then
      Exit(True)
    else if (pnElemento1 = oConexao.Elemento2) and (pnElemento2 = oConexao.Elemento1) then
      Exit(True);
  end;
end;

procedure TNetwork.ValidarValoresElementos(const pnElemento1: Integer; const pnElemento2: Integer);
begin
  // validando intervalo do valor dos elementos
  if (pnElemento1 <= 0) then
    raise TExceptionValorInvalidoElemento.Create('Elemento 1 deve ter um valor maior que zero');
  if (pnElemento2 <= 0) then
    raise TExceptionValorInvalidoElemento.Create('Elemento 2 deve ter um valor maior que zero');
  if (pnElemento1 > FnQuantidadeElementos) then
    raise TExceptionValorInvalidoElemento.Create('Elemento 1 n�o pode ser maior que a quantidade de elementos: ' + FnQuantidadeElementos.ToString);
  if (pnElemento2 > FnQuantidadeElementos) then
    raise TExceptionValorInvalidoElemento.Create('Elemento 2 n�o pode ser maior que a quantidade de elementos: ' + FnQuantidadeElementos.ToString);
  // validando se os elementos s�o iguais
  if (pnElemento1 = pnElemento2) then
    raise TExceptionValoresIguaisElementos.Create('Elementos n�o podem ser iguais');
end;

function TNetwork.Conectar(const pnElemento1: Integer; const pnElemento2: Integer): Boolean;
begin
  ValidarValoresElementos(pnElemento1, pnElemento2);
  if VerificarConexaoJahExiste(pnElemento1, pnElemento2) then
    raise TExceptionConexaoJahExistente.Create('Conex�o j� existente');

  FoListaConexoes.Add(TConexao.Create(pnElemento1, pnElemento2));
  Result := True;
end;

function TNetwork.VerificarElementosEstaoAssociadosIndiretamente(
  const poListaElementosAssociativos: TList<Integer>; const pnElementoComparacao: Integer;
  out psMensagemElementosAssociacao: string): Boolean;
var
  oConexao: TConexao;
  nElementoAssociativo: Integer;
begin
  Result := False;
  psMensagemElementosAssociacao := EmptyStr;

  for oConexao in FoListaConexoes do
  begin
    if (pnElementoComparacao = oConexao.Elemento1) then
    begin
      for nElementoAssociativo in poListaElementosAssociativos do
      begin
        if (nElementoAssociativo = oConexao.Elemento2) then
        begin
          psMensagemElementosAssociacao := Format(cMSG_ELEMENTOS_ASSOCIACAO,
            [oConexao.Elemento1.ToString, oConexao.Elemento2.ToString]);
          Exit(True);
        end;
      end;
    end

    else if (pnElementoComparacao = oConexao.Elemento2) then
    begin
      for nElementoAssociativo in poListaElementosAssociativos do
      begin
        if (nElementoAssociativo = oConexao.Elemento1) then
        begin
          psMensagemElementosAssociacao := Format(cMSG_ELEMENTOS_ASSOCIACAO,
            [oConexao.Elemento1.ToString, oConexao.Elemento2.ToString]);
          Exit(True);
        end;
      end;
    end;
  end;
end;

function TNetwork.Consultar(const pnElemento1: Integer; const pnElemento2: Integer;
  out psMensagem: string): Boolean;
var
  oConexao: TConexao;
  FoListaElementosAssociativos: TList<Integer>;
  sMensagemElementosAssociacao: string;
begin
  Result := False;
  psMensagem := EmptyStr;
  ValidarValoresElementos(pnElemento1, pnElemento2);

  FoListaElementosAssociativos := TList<Integer>.Create;
  try
    for oConexao in FoListaConexoes do
    begin
      if ((pnElemento1 = oConexao.Elemento1) and (pnElemento2 = oConexao.Elemento2)) or
        ((pnElemento1 = oConexao.Elemento2) and (pnElemento2 = oConexao.Elemento1)) then
      begin
        psMensagem := Format(cMSG_ELEMENTOS_CONECTADOS_DIRETAMENTE_CONEXAO,
          [pnElemento1.ToString, pnElemento2.ToString, oConexao.Elemento1.ToString,
          oConexao.Elemento2.ToString]);
        Exit(True);
      end
      else if (pnElemento1 = oConexao.Elemento1) then
        FoListaElementosAssociativos.Add(oConexao.Elemento2)
      else if (pnElemento1 = oConexao.Elemento2) then
        FoListaElementosAssociativos.Add(oConexao.Elemento1);
    end;

    if (FoListaElementosAssociativos.Count > 0) then
    begin
      if VerificarElementosEstaoAssociadosIndiretamente(FoListaElementosAssociativos,
        pnElemento2, sMensagemElementosAssociacao) then
      begin
        psMensagem := Format(cMSG_ELEMENTOS_CONECTADOS_INDIRETAMENTE_CONEXAO,
          [pnElemento1.ToString, pnElemento2.ToString]) + ' ' + sMensagemElementosAssociacao;
        Exit(True);
      end;
    end;
  finally
    FreeAndNil(FoListaElementosAssociativos);
  end;
end;

end.


