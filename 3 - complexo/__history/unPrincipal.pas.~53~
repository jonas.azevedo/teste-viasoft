unit unPrincipal;

{
-> No componente "edQuantidadeItens", poderia setar a propriedade "NumbersOnly"
para "True", garantindo assim que seria digitado um valor num�rico para passar para
a classe TNetwork. N�o o fiz, para atender e testar um dos requisitos do problema:
"Passar um valor inv�lido deve lan�ar uma exce��o.".
}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, uNetwork, Vcl.ExtCtrls;

type
  TfrmPrincipal = class(TForm)
    pnlNetwork: TPanel;
    grBxCriarNetwork: TGroupBox;
    lblQuantidadeItens: TLabel;
    edQuantidadeItens: TEdit;
    btnCriarNetwork: TBitBtn;
    grBxConectarElemento: TGroupBox;
    lblConectarElemento1: TLabel;
    lblConectarElemento2: TLabel;
    edConectarElemento1: TEdit;
    edConectarElemento2: TEdit;
    btnConectar: TBitBtn;
    grBxConsultarConexao: TGroupBox;
    lblConsultarElemento1: TLabel;
    edConsultarElemento1: TEdit;
    edConsultarElemento2: TEdit;
    btnConsultar: TBitBtn;
    lblConsultarElemento2: TLabel;
    pnlOpcoes: TPanel;
    btnLimpar: TBitBtn;
    btnFechar: TBitBtn;
    pnlListarConexoesExistentes: TPanel;
    mmConexoesExistentes: TMemo;
    pnlListarConexoesExistentesBotoes: TPanel;
    btnListarConexoesExistentes: TBitBtn;
    procedure btnCriarNetworkClick(Sender: TObject);
    procedure btnConectarClick(Sender: TObject);
    procedure btnListarConexoesExistentesClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  strict private
    FoNetwork: TNetwork;
  private
    procedure LimparTela;
    procedure HabilitarOpcoesAcessarNetwork(const pbAcessar: Boolean);
    function VerificarPodeCriarNetwork: Boolean;
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses
  System.UITypes, uConexao, uConstantesMensagens, uExceptions;

procedure TfrmPrincipal.LimparTela;
begin
  edQuantidadeItens.Clear;
  edConectarElemento1.Clear;
  edConectarElemento2.Clear;
  edConsultarElemento1.Clear;
  edConsultarElemento2.Clear;
  mmConexoesExistentes.Clear;
  if Assigned(FoNetwork) then
    FreeAndNil(FoNetwork);
  HabilitarOpcoesAcessarNetwork(False);
end;

procedure TfrmPrincipal.HabilitarOpcoesAcessarNetwork(const pbAcessar: Boolean);
begin
  grBxConectarElemento.Enabled := pbAcessar;
  grBxConsultarConexao.Enabled := pbAcessar;
  pnlListarConexoesExistentes.Enabled := pbAcessar;
end;

function TfrmPrincipal.VerificarPodeCriarNetwork: Boolean;
begin
  Result := True;
  if Assigned(FoNetwork) then
  begin
    if MessageDlg('J� existe uma Network criada. Deseja recri�-la?', mtConfirmation,
      [mbYes, mbNo], 0) = mrYes) then
    begin
      LimparTela;
      Exit(True);
    end
    else
      Result := False;
  end;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  LimparTela;
end;

procedure TfrmPrincipal.btnCriarNetworkClick(Sender: TObject);
var
  nQuantidadeItens: Integer;
begin
x
  try
    nQuantidadeItens := StrToIntDef(edQuantidadeItens.Text, 0);
    FoNetwork := TNetwork.Create(nQuantidadeItens);
    MessageDlg(Format(cMSG_NETWORK_CRIADA_COM_ITENS, [nQuantidadeItens.ToString]),
      mtInformation, [mbOk], 0);
      HabilitarOpcoesAcessarNetwork(True);
  except
    on E: TExceptionQuantidadeItensNetwork do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edQuantidadeItens.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmPrincipal.btnConectarClick(Sender: TObject);
var
  nElemento1: Integer;
  nElemento2: Integer;
begin
  nElemento1 := StrToIntDef(edConectarElemento1.Text, 0);
  nElemento2 := StrToIntDef(edConectarElemento2.Text, 0);
  try
    if (FoNetwork.Conectar(nElemento1, nElemento2)) then
      MessageDlg(Format(cMSG_CONEXAO_ENTRE_ELEMENTOS_REALIZADA,
        [nElemento1.ToString, nElemento2.ToString]), mtInformation, [mbOk], 0);
  except
    on E: TExceptionValorInvalidoElemento do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edConectarElemento1.SetFocus;
    end;

    on E: TExceptionValoresIguaisElementos do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edConectarElemento1.SetFocus;
    end;

    on E: TExceptionConexaoJahExistente do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edConectarElemento1.SetFocus;
    end;
  end;
end;

procedure TfrmPrincipal.btnConsultarClick(Sender: TObject);
var
  nElemento1: Integer;
  nElemento2: Integer;
  sMensagem: string;
begin
  nElemento1 := StrToIntDef(edConsultarElemento1.Text, 0);
  nElemento2 := StrToIntDef(edConsultarElemento2.Text, 0);
  try
    if (FoNetwork.Consultar(nElemento1, nElemento2, sMensagem)) then
      MessageDlg(sMensagem, mtInformation, [mbOk], 0)
    else
      MessageDlg(Format(cMSG_NAO_FOI_POSSIVEL_ACHAR_CONEXAO_ENTRE_ELEMENTOS,
        [nElemento1.ToString, nElemento2.ToString]), mtInformation, [mbOk], 0)
  except
    on E: TExceptionValorInvalidoElemento do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edConectarElemento1.SetFocus;
    end;

    on E: TExceptionValoresIguaisElementos do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edConectarElemento1.SetFocus;
    end;

    on E: TExceptionConexaoJahExistente do
    begin
      MessageDlg(E.Message, mtInformation,[mbOK], 0);
      edConectarElemento1.SetFocus;
    end;
  end;
end;

procedure TfrmPrincipal.btnListarConexoesExistentesClick(Sender: TObject);
var
  oConexao: TConexao;
begin
  mmConexoesExistentes.Clear;
  for oConexao in FoNetwork.ListaConexoes do
    mmConexoesExistentes.Lines.Add(oConexao.PegarInformacaoConexao);
end;

procedure TfrmPrincipal.btnLimparClick(Sender: TObject);
begin
  LimparTela;
end;

procedure TfrmPrincipal.btnFecharClick(Sender: TObject);
begin
  Close;
end;

end.
