unit uConexao;

interface

type
  TConexao = class
  strict private
    FnElemento1: Integer;
    FnElemento2: Integer;
  private
    function GetElemento1: Integer;
    function GetElemento2: Integer;
  public
    constructor Create(const pnElemento1: Integer; const pnElemento2: Integer);
    function PegarInformacaoConexao: string;
    property Elemento1: Integer read GetElemento1;
    property Elemento2: Integer read GetElemento2;
  end;

implementation

{ TConexao }

uses
  System.SysUtils;

constructor TConexao.Create(const pnElemento1: Integer; const pnElemento2: Integer);
begin
  FnElemento1 := pnElemento1;
  FnElemento2 := pnElemento2;
end;

function TConexao.GetElemento1: Integer;
begin
  Result := FnElemento1;
end;

function TConexao.GetElemento2: Integer;
begin
  Result := FnElemento2;
end;

function TConexao.PegarInformacaoConexao: string;
begin
  Result := FnElemento1.ToString + ' - ' + FnElemento2.ToString;
end;

end.
