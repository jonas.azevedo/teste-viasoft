# Teste - ViaSoft

Os referidos testes foram feitos utilizando a linguagem de programação Delphi 10 Seattle.

Foi utilizado padronização de código, como por exemplo para a definição de variáveis e parâmetros foi usada a notação húngara.

Exemplo:

-> Variáveis booleans iniciam com a letra "b";

-> Variáveis numéricas iniciam com a letra "n";

-> Variáveis string iniciam com a letra "s";

-> Variáveis de tipos e objetos iniciam com a letra "o".

-> Variáves da classe iniciam com a letra "F" seguido da letra que define o seu tipo. Exemplo: psValor, o qual significa que é uma variável da classe do tipo string;

-> Os parâmetros iniciam com a letra "p" seguido da letra que define o seu tipo. Exemplo: pnValor, o qual significa que é um parâmetro do tipo numérico.


Não foram renomeadas variáveis e tipos que já estavam definidas no projeto antes do desenvolvimento da solução dos referidos testes.